# Package bcftools version 1.11
BCFtools is a set of utilities that manipulate variant calls in the Variant Call Format (VCF) and its binary counterpart BCF.
bcftools Version: 1.11
[https://github.com/samtools/bcftools]

Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH bcftools Version: 1.11
Singularity container based on the recipe: Singularity.bcftools_1.11
## Local build:
```bash
sudo singularity build bcftools_v1.11.sif Singularity.bcftools_1.11
```
## Get image help:

```bash
singularity run-help bcftools_v1.11.sif
```
### Default runscript: bcftools
## Usage:
```bash
./bcftools_v1.11.sif --help
```
or:
```bash
singularity exec bcftools_v1.11.sif bcftools --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull bcftools_v1.11.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/bcftools_v1.11/bcftools_v1.11:latest
```